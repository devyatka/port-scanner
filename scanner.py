import argparse
import socket
import functools
from multiprocessing import Pool


def main():
    with Pool(50) as pool:
        parser = init_arguments()
        if not parser.no_tcp:
            print('TCP scanning results: ')
            pool.map(functools.partial(tcp_scan, host=parser.host), range(parser.start, parser.end))
        if not parser.no_udp:
            print('UDP scanning results: ')
            pool.map(functools.partial(udp_scan, host=parser.host), range(parser.start, parser.end))


def init_arguments():
    parser = argparse.ArgumentParser(prog='TCP/UDP ports scanner',
                                     usage='python3 scanner.py host')
    parser.add_argument('host', type=str,
                        help='domain name or ip address',
                        default=None)
    parser.add_argument('--no-tcp', help='do not use tcp scan', action='store_true')
    parser.add_argument('--no-udp', help='do not use udp scan', action='store_true')
    parser.add_argument('--start', '-s', help='specify port to start from', type=int, default=0)
    parser.add_argument('--end', '-e', help='specify port to end scanning', type=int, default=200)
    parsed = parser.parse_args()
    if parsed.start > parsed.end or parsed.end > 65535 or parsed.start < 0:
        print('Incorrect range of ports to scan')
        exit(1)
    return parsed


def udp_scan(port, host):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(1)
    try:
        sock.connect((host, port))
        # for sntp send: b'\x1b' + b'\x00 * 47'
        sock.send(b"\x00\x01\x01\x00\x00\x01\x00\x00\x00\x00\x00\x00\x01\x31\x01\x30\x01\x30\x01\x30\x01\x30\x01"
                  b"\x30\x01\x30\x01\x30\x01\x30\x01\x30\x01\x30\x01\x30\x01\x30\x01\x30\x01\x30\x01\x30\x01\x30"
                  b"\x01\x30\x01\x30\x01\x30\x01\x30\x01\x30\x01\x30\x01\x30\x01\x30\x01\x30\x01\x30\x01\x30\x01"
                  b"\x30\x01\x38\x01\x65\x01\x66\x03\x69\x70\x36\x04\x61\x72\x70\x61\x00\x00\x0c\x00\x01")
        if sock.recv(1024):
            print(f'\tUDP port {port} is opened (protocol DNS)')
    except (socket.timeout, socket.error):
        pass
    finally:
        sock.close()


def tcp_scan(port, host):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(0.5)
    try:
        status = sock.connect_ex((host, port))
        if status == 0:
            print(f'\tTCP port {port} is opened')
        elif status == 111:
            print(f'\tTCP port {port} is closed')
        else:
            return
    except:
        pass
    finally:
        sock.close()


if __name__ == '__main__':
    main()
